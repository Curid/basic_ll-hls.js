import path from "path";
import { defineConfig } from "vite";

export default defineConfig({
	build: {
		lib: {
			entry: path.resolve(__dirname, "src/hls.ts"),
			name: "hls",
			formats: ["es", "cjs"],
			fileName: (format) => {
				if (format === "cjs") {
					return "hls.cjs";
				}
				return "hls.js";
			},
		},
	},
});

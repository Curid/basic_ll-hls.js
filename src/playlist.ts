/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

export interface playlist {
	version: number;
	independentSegments?: boolean;
	partInformation?: boolean;
	serverControl?: serverControl;
	init?: string;
	codecs?: string;
	parts: part[];
	mediaURI?: string;
	hint?: preloadHint;
}

export interface playlistFast {
	parts: part[];
	hint?: preloadHint;
}

export interface preloadHint {
	msn: number;
	index: number;
}

export interface part {
	uri: string;
	//duration: number;
	independent?: boolean;
}

export interface serverControl {
	canSkipUntil?: number;
	partHoldBack?: number;
	canBlockReload?: boolean;
}

// parsePlaylist parses a m3u8 playlist.
export function parsePlaylist(input: string): playlist {
	if (!input.startsWith("#EXTM3U")) {
		throw `playlist must start with '#EXTM3U': ${input}`;
	}

	const playlist: playlist = {
		version: -1,
		parts: [],
	};

	let msn = 0;
	let partIndex = 0;

	for (const line of input.split("\n")) {
		if (line.startsWith("#EXT-X-VERSION:")) {
			playlist.version = Number(readAttributes(line)[0]);
			if (!playlist.version) {
				throw `${line} parse attribute`;
			}
		} else if (line.startsWith("#EXT-X-INDEPENDENT-SEGMENTS")) {
			playlist.independentSegments = true;
		} else if (line.startsWith("#EXT-X-MEDIA-SEQUENCE:")) {
			const mediaSequence = Number(readAttributes(line)[0]);
			if (mediaSequence !== 0 && !mediaSequence) {
				throw `${line} invalid mediaSequence`;
			}
			msn = mediaSequence;
		} else if (line.startsWith("#EXT-X-PART-INF:")) {
			playlist.partInformation = true;
		} else if (line.startsWith("#EXT-X-SERVER-CONTROL:")) {
			const attrs = mapAttributes(line);
			const canSkipUntil = Number(attrs["CAN-SKIP-UNTIL"]);
			const partHoldBack = Number(attrs["PART-HOLD-BACK"]);
			playlist.serverControl = {
				canSkipUntil: canSkipUntil ? canSkipUntil : undefined,
				partHoldBack: partHoldBack ? partHoldBack : undefined,
				canBlockReload: attrs["CAN-BLOCK-RELOAD"] === "YES" ? true : undefined,
			};
			/*} else if (line.startsWith("#EXTINF")) {
			const duration = Number(readAttributes(line)[0]);
			if (!duration) {
				throw `${line} invalid duration`;
			}
			segBuf.duration = duration;*/
		} else if (line.startsWith("#EXT-X-MAP:")) {
			playlist.init = mapAttributes(line)["URI"];
		} else if (line.startsWith("#EXT-X-PART:")) {
			const attrs = mapAttributes(line);
			const part: part = {
				uri: attrs["URI"],
				//duration: Number(attrs["DURATION"]),
			};
			if (attrs["INDEPENDENT"] === "YES") {
				part.independent = true;
			}
			playlist.parts.push(part);
			partIndex++;
		} else if (line.startsWith("#EXT-X-PRELOAD-HINT:")) {
			playlist.hint = { msn: msn, index: partIndex };
		} else if (line.startsWith("#EXT-X-SKIP:")) {
			const skipped = Number(mapAttributes(line)["SKIPPED-SEGMENTS"]);
			if (skipped !== 0 && !skipped) {
				throw `${line} invalid SKIPPED-SEGMENTS`;
			}
			msn += skipped;
		} else if (line.startsWith("#EXT-X-STREAM-INF:")) {
			const attrs = mapAttributes(line);
			playlist.codecs = attrs["CODECS"];
		} else if (line.startsWith("#")) {
			continue;
		} else if (line) {
			if (line.endsWith(".m3u8")) {
				playlist.mediaURI = line;
			} else {
				msn++;
				partIndex = 0;
			}
		}
	}

	return playlist;
}

function readAttributes(line: string) {
	return line.split(":")[1].split(",");
}

function mapAttributes(line: string) {
	const map: { [key: string]: string } = {};
	const input = line.split(":")[1];
	let keyBuf = "";
	let inQuotes = false;
	let [l, r] = [0, 0];
	while (r <= input.length) {
		const char = input.charAt(r);
		if (char === "=") {
			keyBuf = input.slice(l, r);
			l = r + 1;
		} else if ((char === "," && !inQuotes) || r == input.length) {
			map[keyBuf] = input.slice(l, r);
			l = r = r + 1;
		} else if (char === '"') {
			if (inQuotes) {
				map[keyBuf] = input.slice(l, r);
				l = r = r + 2;
			} else {
				l++;
			}
			inQuotes = !inQuotes;
		}
		r++;
	}
	return map;
}

// parsePlaylistFast parses the minimum amount of data.
export function parsePlaylistFast(input: string): playlistFast {
	if (!input.startsWith("#EXTM3U")) {
		throw `playlist must start with '#EXTM3U': ${input}`;
	}

	const playlist: playlistFast = {
		parts: [],
	};

	let msn = 0;
	let partIndex = 0;

	for (const line of input.split("\n")) {
		if (line.startsWith("#EXT-X-MEDIA-SEQUENCE")) {
			msn = Number(readAttributes(line)[0]);
		} else if (line.startsWith("#EXT-X-PART:")) {
			const attrs = mapAttributesFast(line);
			partIndex++;
			const part: part = {
				uri: attrs["URI"],
				//duration: Number(attrs["DURATION"]),
			};
			playlist.parts.push(part);
		} else if (line.startsWith("#EXT-X-PRELOAD-HINT:")) {
			playlist.hint = { msn: msn, index: partIndex };
		} else if (line.startsWith("#EXT-X-SKIP")) {
			const skipped = Number(mapAttributesFast(line)["SKIPPED-SEGMENTS"]);
			msn += skipped;
		} else if (line.startsWith("#")) {
			continue;
		} else if (line && line.endsWith(".mp4")) {
			msn++;
			partIndex = 0;
		}
	}
	return playlist;
}

function mapAttributesFast(line: string) {
	const map: { [key: string]: string } = {};
	const attrs = line.split(":")[1].split(",");
	for (const attr of attrs) {
		const [key, val] = attr.split("=");
		if (val.startsWith('"')) {
			map[key] = val.slice(1, -1);
		} else {
			map[key] = val;
		}
	}
	return map;
}

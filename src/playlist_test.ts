/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import * as p from "./playlist.js";

// Specification.
// https://datatracker.ietf.org/doc/html/draft-pantos-hls-rfc8216bis
describe("parsePlaylist", () => {
	const cases: {
		name: string;
		input: string;
		expected?: p.playlist;
		shouldThrow?: boolean;
	}[] = [
		// 4.1 Definition of a Playlist.
		{
			name: "missingEXTM3U",
			input: "#EXT-X-VERSION:1",
			shouldThrow: true,
		},
		{
			name: "emptyLines",
			input: "#EXTM3U\n\n\n#EXT-X-VERSION:1",
			expected: { version: 1, parts: [] },
		},

		{
			name: "playlistURI",
			input: "#EXTM3U\n#EXT-X-VERSION:1\nx.m3u8",
			expected: { version: 1, parts: [], mediaURI: "x.m3u8" },
		},
		{
			name: "comment",
			input: "#EXTM3U\n#EXT-X-VERSION:1\n#x",
			expected: { version: 1, parts: [] },
		},
		{
			name: "commentWithSpaces",
			input: "#EXTM3U\n#EXT-X-VERSION:1\n#x x ",
			expected: { version: 1, parts: [] },
		},

		{
			name: "playlist",
			input: "#EXTM3U\n#EXT-X-VERSION:1\n\nx.m3u8\n#comment",
			expected: { version: 1, parts: [], mediaURI: "x.m3u8" },
		},
		{
			name: "multipleColons",
			input: "#EXTM3U\n#EXT-X-VERSION:1:",
			expected: { version: 1, parts: [] },
		},
		// 4.4.1.2 EXT-X-VERSION.
		{
			name: "version",
			input: "#EXTM3U\n#EXT-X-VERSION:1",
			expected: { version: 1, parts: [] },
		},
		{
			name: "versionMinimum",
			input: "#EXTM3U\n#EXT-X-VERSION:0",
			shouldThrow: true,
		},
		{
			name: "versionMultiple",
			input: "#EXTM3U\n#EXT-X-VERSION:1,2",
			expected: { version: 1, parts: [] },
		},
		// 4.4.2.1 EXT-X-INDEPENDENT-SEGMENTS.
		{
			name: "independentSegments",
			input: "#EXTM3U\n#EXT-X-VERSION:1\n#EXT-X-INDEPENDENT-SEGMENTS",
			expected: { version: 1, independentSegments: true, parts: [] },
		},
		// 4.4.3.2 EXT-X-MEDIA-SEQUENCE.
		{
			name: "mediaSequence",
			input:
				"#EXTM3U\n" +
				"#EXT-X-VERSION:1\n" +
				"#EXT-X-MEDIA-SEQUENCE:5\n" +
				'#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part0.mp4"\n' +
				"x.mp4",
			expected: {
				version: 1,
				parts: [],
				hint: { msn: 5, index: 0 },
			},
		},
		{
			name: "mediaSequenceZero",
			input:
				"#EXTM3U\n" +
				"#EXT-X-VERSION:1\n" +
				"#EXT-X-MEDIA-SEQUENCE:0\n" +
				'#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part0.mp4"\n' +
				"x.mp4",
			expected: {
				version: 1,
				parts: [],
				hint: { msn: 0, index: 0 },
			},
		},
		// 4.4.3.7 EXT-X-PART-INF.
		{
			name: "partInformation",
			input: "#EXTM3U\n#EXT-X-VERSION:1\n#EXT-X-PART-INF:PART-TARGET=x",
			expected: { version: 1, partInformation: true, parts: [] },
		},
		// 4.4.3.8 EXT-X-SERVER-CONTROL.
		{
			name: "canSkipUntil",
			input:
				"#EXTM3U\n" +
				"#EXT-X-VERSION:1\n" +
				"#EXT-X-SERVER-CONTROL:CAN-SKIP-UNTIL=2.3",
			expected: {
				version: 1,
				serverControl: { canSkipUntil: 2.3 },
				parts: [],
			},
		},
		{
			name: "partHoldBack",
			input:
				"#EXTM3U\n" +
				"#EXT-X-VERSION:1\n" +
				"#EXT-X-SERVER-CONTROL:PART-HOLD-BACK=2.3",
			expected: {
				version: 1,
				serverControl: { partHoldBack: 2.3 },
				parts: [],
			},
		},
		{
			name: "partHoldBack",
			input:
				"#EXTM3U\n" +
				"#EXT-X-VERSION:1\n" +
				"#EXT-X-SERVER-CONTROL:CAN-BLOCK-RELOAD=YES",
			expected: {
				version: 1,
				serverControl: { canBlockReload: true },
				parts: [],
			},
		},
		{
			name: "serverControl",
			input:
				"#EXTM3U\n" +
				"#EXT-X-VERSION:1\n" +
				"#EXT-X-SERVER-CONTROL:CAN-SKIP-UNTIL=2.3,PART-HOLD-BACK=4.5,CAN-BLOCK-RELOAD=YES",
			expected: {
				version: 1,
				serverControl: {
					canSkipUntil: 2.3,
					partHoldBack: 4.5,
					canBlockReload: true,
				},
				parts: [],
			},
		},
		// 4.4.4.5 EXT-X-MAP.
		{
			name: "init",
			input: "#EXTM3U\n" + "#EXT-X-VERSION:1\n" + '#EXT-X-MAP:URI="init.mp4"\n',
			expected: { version: 1, init: "init.mp4", parts: [] },
		},

		// 4.4.4.9 EXT-X-PART.
		{
			name: "part",
			input:
				"#EXTM3U\n" +
				"#EXT-X-VERSION:1\n" +
				'#EXT-X-PART:DURATION=2.3,URI="4"\n' +
				"#EXTINF:5.6\n" +
				"x.mp4",
			expected: { version: 1, parts: [{ uri: "4" }] },
		},
		{
			name: "partIndependent",
			input:
				"#EXTM3U\n" +
				"#EXT-X-VERSION:1\n" +
				'#EXT-X-PART:DURATION=2.3,URI="4",INDEPENDENT=YES\n' +
				"#EXTINF:5.6\n" +
				"x.mp4",
			expected: {
				version: 1,
				parts: [{ uri: "4", independent: true }],
			},
		},
		// 4.4.5.1 EXT-X-PRELOAD-HINT.
		{
			name: "preloadHint",
			input:
				"#EXTM3U\n" +
				"#EXT-X-VERSION:1\n" +
				'#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part.mp4"\n' +
				"x.mp4",
			expected: {
				version: 1,
				parts: [],
				hint: { msn: 0, index: 0 },
			},
		},
		// 4.4.5.2 EXT-X-SKIP.
		{
			name: "skip",
			input:
				"#EXTM3U\n" +
				"#EXT-X-VERSION:1\n" +
				"#EXT-X-MEDIA-SEQUENCE:3\n" +
				"#EXT-X-SKIP:SKIPPED-SEGMENTS=2\n" +
				'#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part0.mp4"\n' +
				"x.mp4",
			expected: {
				version: 1,
				parts: [],
				hint: { msn: 5, index: 0 },
			},
		},
		// 4.4.6.2 EXT-X-STREAM-INF.
		{
			name: "codec",
			input:
				"#EXTM3U\n" +
				"#EXT-X-VERSION:1\n" +
				'#EXT-X-STREAM-INF:BANDWIDTH=1,CODECS="a,b"',
			expected: { version: 1, codecs: "a,b", parts: [] },
		},
		// Real examples.
		{
			name: "multiVariantPlaylist",
			input: `#EXTM3U
#EXT-X-VERSION:9
#EXT-X-INDEPENDENT-SEGMENTS

#EXT-X-STREAM-INF:BANDWIDTH=200000,CODECS="avc1.640016,mp4a.40.2"
stream.m3u8`,
			expected: {
				version: 9,
				independentSegments: true,
				codecs: "avc1.640016,mp4a.40.2",
				parts: [],
				mediaURI: "stream.m3u8",
			},
		},
		{
			name: "mediaPlaylist",
			input: `#EXTM3U
#EXT-X-VERSION:9
#EXT-X-TARGETDURATION:4
#EXT-X-SERVER-CONTROL:CAN-BLOCK-RELOAD=YES,PART-HOLD-BACK=1.45833,CAN-SKIP-UNTIL=24
#EXT-X-PART-INF:PART-TARGET=0.583333334
#EXT-X-MEDIA-SEQUENCE:2
#EXT-X-SKIP:SKIPPED-SEGMENTS=1
#EXT-X-MAP:URI="init.mp4"

#EXTINF:4.00000,
seg3.mp4
#EXT-X-PROGRAM-DATE-TIME:2022-07-01T01:24:49.267Z
#EXT-X-PART:DURATION=0.50000,URI="part9.mp4",INDEPENDENT=YES
#EXT-X-PART:DURATION=0.50000,URI="part10.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part11.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part12.mp4"
#EXT-X-PART:DURATION=0.58333,URI="part13.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part14.mp4"
#EXT-X-PART:DURATION=0.58333,URI="part15.mp4"
#EXT-X-PART:DURATION=0.33333,URI="part16.mp4"
#EXTINF:3.83333,
seg4.mp4
#EXT-X-PROGRAM-DATE-TIME:2022-07-01T01:24:53.268Z
#EXT-X-PART:DURATION=0.58333,URI="part17.mp4",INDEPENDENT=YES
#EXT-X-PART:DURATION=0.50000,URI="part18.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part19.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part20.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part21.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part22.mp4"
#EXT-X-PART:DURATION=0.58333,URI="part23.mp4"
#EXT-X-PART:DURATION=0.33333,URI="part24.mp4"
#EXTINF:3.83333,
seg5.mp4
#EXT-X-PART:DURATION=0.50000,URI="part25.mp4",INDEPENDENT=YES
#EXT-X-PART:DURATION=0.50000,URI="part26.mp4"
#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part27.mp4"`,
			expected: {
				version: 9,
				partInformation: true,
				serverControl: {
					canSkipUntil: 24,
					partHoldBack: 1.45833,
					canBlockReload: true,
				},
				init: "init.mp4",
				parts: [
					{ uri: "part9.mp4", independent: true },
					{ uri: "part10.mp4" },
					{ uri: "part11.mp4" },
					{ uri: "part12.mp4" },
					{ uri: "part13.mp4" },
					{ uri: "part14.mp4" },
					{ uri: "part15.mp4" },
					{ uri: "part16.mp4" },
					{ uri: "part17.mp4", independent: true },
					{ uri: "part18.mp4" },
					{ uri: "part19.mp4" },
					{ uri: "part20.mp4" },
					{ uri: "part21.mp4" },
					{ uri: "part22.mp4" },
					{ uri: "part23.mp4" },
					{ uri: "part24.mp4" },
					{ uri: "part25.mp4", independent: true },
					{ uri: "part26.mp4" },
				],
				hint: { msn: 6, index: 2 },
			},
		},
	];

	for (const tc of cases) {
		test(tc.name, () => {
			if (tc.shouldThrow) {
				expect(() => {
					p.parsePlaylist(tc.input);
				}).toThrow();
			} else {
				const actual = p.parsePlaylist(tc.input);
				expect(actual).toEqual(tc.expected);
			}
		});
	}
});

describe("parsePlaylistFast", () => {
	const cases: {
		name: string;
		input: string;
		expected?: p.playlistFast;
		shouldThrow?: boolean;
	}[] = [
		// 4.1 Definition of a Playlist.
		{
			name: "missingEXTM3U",
			input: "#EXT-X-VERSION:1",
			shouldThrow: true,
		},
		{
			name: "emptyLines",
			input: "#EXTM3U\n\n\n#EXT-X-VERSION:1",
			expected: { parts: [] },
		},
		{
			name: "comment",
			input: "#EXTM3U\n#EXT-X-VERSION:1\n#x",
			expected: { parts: [] },
		},
		{
			name: "commentWithSpaces",
			input: "#EXTM3U\n#EXT-X-VERSION:1\n#x x ",
			expected: { parts: [] },
		},
		{
			name: "multipleColons",
			input: "#EXTM3U\n#EXT-X-VERSION:1:",
			expected: { parts: [] },
		},
		// 4.4.1.2 EXT-X-VERSION.
		{
			name: "version",
			input: "#EXTM3U\n#EXT-X-VERSION:1",
			expected: { parts: [] },
		},
		{
			name: "versionMinimum",
			input: "#EXTM3U\n#EXT-X-VERSION:0",
			shouldThrow: true,
		},
		{
			name: "versionMultiple",
			input: "#EXTM3U\n#EXT-X-VERSION:1,2",
			expected: { parts: [] },
		},
		// 4.4.3.2 EXT-X-MEDIA-SEQUENCE.
		{
			name: "mediaSequence",
			input:
				"#EXTM3U\n" +
				"#EXT-X-VERSION:1\n" +
				"#EXT-X-MEDIA-SEQUENCE:5\n" +
				'#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part0.mp4"\n' +
				"x.mp4",
			expected: {
				parts: [],
				hint: { msn: 5, index: 0 },
			},
		},
		{
			name: "mediaSequenceZero",
			input:
				"#EXTM3U\n" +
				"#EXT-X-VERSION:1\n" +
				"#EXT-X-MEDIA-SEQUENCE:0\n" +
				'#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part0.mp4"\n' +
				"x.mp4",
			expected: {
				parts: [],
				hint: { msn: 0, index: 0 },
			},
		},
		// 4.4.4.9 EXT-X-PART.
		{
			name: "part",
			input:
				"#EXTM3U\n" +
				"#EXT-X-VERSION:1\n" +
				'#EXT-X-PART:DURATION=2.3,URI="4"\n' +
				"#EXTINF:5.6\n" +
				"x.mp4",
			expected: { parts: [{ uri: "4" }] },
		},
		// 4.4.5.1 EXT-X-PRELOAD-HINT.
		{
			name: "preloadHint",
			input:
				"#EXTM3U\n" +
				"#EXT-X-VERSION:1\n" +
				'#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part.mp4"\n' +
				"x.mp4",
			expected: {
				parts: [],
				hint: { msn: 0, index: 0 },
			},
		},
		// 4.4.5.2 EXT-X-SKIP.
		{
			name: "skip",
			input:
				"#EXTM3U\n" +
				"#EXT-X-VERSION:1\n" +
				"#EXT-X-MEDIA-SEQUENCE:3\n" +
				"#EXT-X-SKIP:SKIPPED-SEGMENTS=2\n" +
				'#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part0.mp4"\n' +
				"x.mp4",
			expected: {
				parts: [],
				hint: { msn: 5, index: 0 },
			},
		},
		{
			name: "mediaPlaylist",
			input: `#EXTM3U
#EXT-X-VERSION:9
#EXT-X-TARGETDURATION:4
#EXT-X-SERVER-CONTROL:CAN-BLOCK-RELOAD=YES,PART-HOLD-BACK=1.45833,CAN-SKIP-UNTIL=24
#EXT-X-PART-INF:PART-TARGET=0.583333334
#EXT-X-MEDIA-SEQUENCE:2
#EXT-X-SKIP:SKIPPED-SEGMENTS=1
#EXT-X-MAP:URI="init.mp4"

#EXTINF:4.00000,
seg3.mp4
#EXT-X-PROGRAM-DATE-TIME:2022-07-01T01:24:49.267Z
#EXT-X-PART:DURATION=0.50000,URI="part9.mp4",INDEPENDENT=YES
#EXT-X-PART:DURATION=0.50000,URI="part10.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part11.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part12.mp4"
#EXT-X-PART:DURATION=0.58333,URI="part13.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part14.mp4"
#EXT-X-PART:DURATION=0.58333,URI="part15.mp4"
#EXT-X-PART:DURATION=0.33333,URI="part16.mp4"
#EXTINF:3.83333,
seg4.mp4
#EXT-X-PROGRAM-DATE-TIME:2022-07-01T01:24:53.268Z
#EXT-X-PART:DURATION=0.58333,URI="part17.mp4",INDEPENDENT=YES
#EXT-X-PART:DURATION=0.50000,URI="part18.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part19.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part20.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part21.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part22.mp4"
#EXT-X-PART:DURATION=0.58333,URI="part23.mp4"
#EXT-X-PART:DURATION=0.33333,URI="part24.mp4"
#EXTINF:3.83333,
seg5.mp4
#EXT-X-PART:DURATION=0.50000,URI="part25.mp4",INDEPENDENT=YES
#EXT-X-PART:DURATION=0.50000,URI="part26.mp4"
#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part27.mp4"`,
			expected: {
				parts: [
					{ uri: "part9.mp4" },
					{ uri: "part10.mp4" },
					{ uri: "part11.mp4" },
					{ uri: "part12.mp4" },
					{ uri: "part13.mp4" },
					{ uri: "part14.mp4" },
					{ uri: "part15.mp4" },
					{ uri: "part16.mp4" },
					{ uri: "part17.mp4" },
					{ uri: "part18.mp4" },
					{ uri: "part19.mp4" },
					{ uri: "part20.mp4" },
					{ uri: "part21.mp4" },
					{ uri: "part22.mp4" },
					{ uri: "part23.mp4" },
					{ uri: "part24.mp4" },
					{ uri: "part25.mp4" },
					{ uri: "part26.mp4" },
				],
				hint: { msn: 6, index: 2 },
			},
		},
	];

	for (const tc of cases) {
		test(tc.name, () => {
			if (tc.shouldThrow) {
				expect(() => {
					p.parsePlaylist(tc.input);
				}).toThrow();
			} else {
				const actual = p.parsePlaylistFast(tc.input);
				expect(actual).toEqual(tc.expected);
			}
		});
	}
});

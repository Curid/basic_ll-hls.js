/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

class sourceBuffer {
	$video!: video;
	onupdateend!: () => void;
	appendBuffer(buf: ArrayBuffer) {
		/* eslint-disable-next-line compat/compat */
		const decoded = new TextDecoder().decode(buf);
		if (decoded !== "init") {
			const end = this.$video.buffered.end();
			this.$video.buffered.setEnd(end + 200);
			console.log(this.$video.buffered.endTS);
		}
		this.onupdateend();
		// @ts-expect-error
		return new sourceBuffer() as SourceBuffer;
	}
	/* eslint-disable @typescript-eslint/no-unused-vars, @typescript-eslint/no-empty-function */
	remove() {}
	setVideo($video: video) {
		this.$video = $video;
	}
	get buffered() {
		return this.$video.buffered;
	}
}

export class mediaSource {
	sourceBuffer = new sourceBuffer();
	addSourceBuffer(): sourceBuffer {
		return this.sourceBuffer;
	}
	removeSourceBuffer() {}
	endOfStream() {}
	/* eslint-enable @typescript-eslint/no-unused-vars, @typescript-eslint/no-empty-function */
	onsourceopen!: () => void;
	setVideo($video: video) {
		console.log("x", $video);
		this.sourceBuffer.setVideo($video);
	}
}

export function createObjectURL(mediaSource: MediaSource | Blob) {
	mediaSource = mediaSource as MediaSource;
	if (mediaSource.onsourceopen) {
		mediaSource.onsourceopen(new Event(""));
	}
	return mediaSource;
}

export class video {
	paused = true;
	currentTime = 0;
	buffered = new timeRanges();
	play() {
		this.paused = false;
	}
	set src(mediaSource: mediaSource) {
		mediaSource.setVideo(this);
	}
}

class timeRanges {
	endTS = 0;
	end() {
		return this.endTS;
	}
	setEnd(end: number) {
		this.endTS = end;
	}
}

interface pendingRequest {
	url: string;
	resolve: (value: Response) => void;
	signal: AbortSignal;
}
interface pendingResponse {
	url: string;
	response: Response;
	resolve: (value: void) => void;
}

class response {
	response: string;
	constructor(response: string) {
		this.response = response;
	}

	text() {
		return this.response;
	}
	arrayBuffer(): ArrayBuffer {
		/* eslint-disable-next-line compat/compat */
		return new TextEncoder().encode(this.response).buffer;
	}
}

export function newFetch() {
	let pendingRequest: pendingRequest | undefined;
	let pendingResponse: pendingResponse | undefined;

	return {
		once: async (url: string, res: string) => {
			return new Promise<void>((resolve) => {
				if (pendingRequest === undefined) {
					if (pendingResponse) {
						throw "multiple pending responses";
					}
					pendingResponse = {
						url: url.toString(),
						// @ts-expect-error
						response: new response(res) as Response,
						resolve: resolve,
					};
					return;
				}
				if (pendingRequest.url !== url) {
					throw `\nurl missmatch:\nexpected: ${url}\ngot:      ${pendingRequest.url}\n`;
				}
				const temp = pendingRequest;
				pendingRequest = undefined;
				// @ts-expect-error
				temp.resolve(new response(res) as Response);
				resolve();
			});
		},

		mockFetch: (
			url: RequestInfo | URL,
			init: RequestInit | undefined
		): Promise<Response> => {
			return new Promise<Response>((resolve) => {
				if (!init || !init.signal) {
					throw "signal must be included in fetch";
				}
				if (pendingRequest) {
					throw `\nmultiple pending requests:\n${pendingRequest.url}\n${url}`;
				}
				if (pendingResponse) {
					if (pendingResponse.url !== url) {
						throw `\nurl missmatch:\nexpected: ${pendingResponse.url}\ngot:      ${url}\n`;
					}
					const temp = pendingResponse;
					pendingResponse = undefined;
					temp.resolve();
					resolve(temp.response);
					return;
				}

				pendingRequest = {
					url: url.toString(),
					resolve: resolve,
					signal: init.signal,
				};
			});
		},
		reset() {
			pendingRequest = undefined;
		},
	};
}

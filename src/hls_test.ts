/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import { jest } from "@jest/globals";
import * as mock from "./mock.js";

import Hls, {
	config,
	missingCodecsError,
	minVersionError,
	missingMediaURIError,
	failedAfterRecoveryError,
	noNextPartError,
} from "./hls.js";

// @ts-expect-error
global.MediaSource = mock.mediaSource;
// @ts-expect-error
global.URL.createObjectURL = mock.createObjectURL;
global.fetch = mock.newFetch().mockFetch;

const testConfig: config = {
	maxDelaySec: 500,
	recoverySleepSec: 0,
};

describe("hls", () => {
	test("ok", async () => {
		const $video = new mock.video();
		const fetch = mock.newFetch();
		jest.spyOn(global, "fetch").mockImplementation(fetch.mockFetch);

		const hls = new Hls(testConfig);
		hls.onError = (error) => {
			console.log(error);
		};
		// @ts-expect-error
		hls.init($video, "x/index.m3u8");
		await fetch.once(
			"x/index.m3u8",
			"#EXTM3U\n" +
				"#EXT-X-VERSION:9\n" +
				'#EXT-X-STREAM-INF:BANDWIDTH=200000,CODECS="avc1.640016,mp4a.40.2"\n' +
				"stream.m3u8"
		);
		await fetch.once(
			"x/stream.m3u8",
			`#EXTM3U
#EXT-X-VERSION:9
#EXT-X-SKIP:SKIPPED-SEGMENTS=2
#EXT-X-MAP:URI="init.mp4"
#EXT-X-PART:DURATION=200,URI="part9.mp4",INDEPENDENT=YES
#EXT-X-PART:DURATION=200,URI="part10.mp4"
#EXT-X-PART:DURATION=200,URI="part11.mp4"
#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part12.mp4"`
		);
		await fetch.once("x/init.mp4", "init");
		await fetch.once("x/part9.mp4", "");
		await fetch.once("x/part10.mp4", "");
		expect($video.buffered.end()).toBe(200);
		await fetch.once("x/part11.mp4", "");
		expect($video.buffered.end()).toBe(400);
		expect($video.currentTime).toBe(0);
		await fetch.once(
			"x/stream.m3u8?_HLS_msn=2&_HLS_part=3&_HLS_skip=YES",
			`#EXTM3U
#EXT-X-VERSION:1
#EXT-X-SKIP:SKIPPED-SEGMENTS=2
#EXT-X-PART:DURATION=200,URI="part9.mp4",INDEPENDENT=YES
#EXT-X-PART:DURATION=200,URI="part10.mp4"
#EXT-X-PART:DURATION=200,URI="part11.mp4"
#EXT-X-PART:DURATION=200,URI="part12.mp4"
#EXT-X-PART:DURATION=200,URI="part13.mp4"
#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part14.mp4"`
		);
		expect($video.buffered.end()).toBe(600);
		await fetch.once("x/part12.mp4", "");
		await fetch.once("x/part13.mp4", "");
		expect($video.currentTime).toBe(300);
		hls.destroy();
	});

	test("startFromLastIndependantPart", async () => {
		const $video = new mock.video();
		const fetch = mock.newFetch();
		jest.spyOn(global, "fetch").mockImplementation(fetch.mockFetch);

		const hls = new Hls(testConfig);
		hls.onError = (error) => {
			console.log(error);
		};

		// @ts-expect-error
		hls.init($video, "x/index.m3u8");
		await fetch.once(
			"x/index.m3u8",
			"#EXTM3U\n" +
				"#EXT-X-VERSION:9\n" +
				'#EXT-X-STREAM-INF:BANDWIDTH=200000,CODECS="avc1.640016,mp4a.40.2"\n' +
				"stream.m3u8"
		);
		await fetch.once(
			"x/stream.m3u8",
			`#EXTM3U
#EXT-X-VERSION:9
#EXT-X-SKIP:SKIPPED-SEGMENTS=2
#EXT-X-MAP:URI="init.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part9.mp4",INDEPENDENT=YES
#EXT-X-PART:DURATION=0.50000,URI="part10.mp4"
#EXTINF:3.83333,
seg4.mp4

#EXT-X-PART:DURATION=0.33333,URI="part16.mp4"
#EXT-X-PART:DURATION=0.58333,URI="part17.mp4",INDEPENDENT=YES
#EXT-X-PART:DURATION=0.50000,URI="part18.mp4"

#EXTINF:3.83333,
seg5.mp4
#EXT-X-PART:DURATION=0.58333,URI="part23.mp4"
#EXT-X-PART:DURATION=0.33333,URI="part24.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part25.mp4",INDEPENDENT=YES
#EXT-X-PART:DURATION=0.50000,URI="part26.mp4"
#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part27.mp4"`
		);
		await fetch.once("x/init.mp4", "init");
		await fetch.once("x/part25.mp4", "");
		await fetch.once("x/part26.mp4", "");
		await fetch.once(
			"x/stream.m3u8?_HLS_msn=4&_HLS_part=4&_HLS_skip=YES",
			`#EXTM3U
#EXT-X-VERSION:9
#EXT-X-SKIP:SKIPPED-SEGMENTS=4
#EXT-X-PART:DURATION=0.58333,URI="part23.mp4"
#EXT-X-PART:DURATION=0.33333,URI="part24.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part25.mp4",INDEPENDENT=YES
#EXT-X-PART:DURATION=0.50000,URI="part26.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part27.mp4"
#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part28.mp4"`
		);
		await fetch.once("x/part27.mp4", "");
		hls.destroy();
	});

	test("noNextPart", async () => {
		const $video = new mock.video();
		const fetch = mock.newFetch();
		jest.spyOn(global, "fetch").mockImplementation(fetch.mockFetch);

		const hls = new Hls(testConfig);

		// @ts-expect-error
		hls.init($video, "x/index.m3u8");
		await fetch.once(
			"x/index.m3u8",
			"#EXTM3U\n" +
				"#EXT-X-VERSION:9\n" +
				'#EXT-X-STREAM-INF:BANDWIDTH=200000,CODECS="avc1.640016,mp4a.40.2"\n' +
				"stream.m3u8"
		);
		await fetch.once(
			"x/stream.m3u8",
			`#EXTM3U
#EXT-X-VERSION:9
#EXT-X-MAP:URI="init.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part9.mp4",INDEPENDENT=YES
#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part10.mp4"`
		);
		await fetch.once("x/init.mp4", "init");
		await fetch.once("x/part9.mp4", "");
		await fetch.once(
			"x/stream.m3u8?_HLS_msn=0&_HLS_part=1&_HLS_skip=YES",
			`#EXTM3U
#EXT-X-VERSION:9
#EXT-X-SKIP:SKIPPED-SEGMENTS=4
#EXT-X-PART:DURATION=0.50000,URI="part9.mp4",INDEPENDENT=YES`
		);
		expect(await hls.waitForError()).toEqual(noNextPartError);
		hls.destroy();
	});

	test("loadAllNewParts", async () => {
		const $video = new mock.video();
		const fetch = mock.newFetch();
		jest.spyOn(global, "fetch").mockImplementation(fetch.mockFetch);

		const hls = new Hls(testConfig);
		hls.onError = (error) => {
			console.log(error);
		};

		// @ts-expect-error
		hls.init($video, "x/index.m3u8");
		await fetch.once(
			"x/index.m3u8",
			"#EXTM3U\n" +
				"#EXT-X-VERSION:9\n" +
				'#EXT-X-STREAM-INF:BANDWIDTH=200000,CODECS="avc1.640016,mp4a.40.2"\n' +
				"stream.m3u8"
		);
		await fetch.once(
			"x/stream.m3u8",
			`#EXTM3U
#EXT-X-VERSION:9
#EXT-X-MAP:URI="init.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part9.mp4",INDEPENDENT=YES
#EXT-X-PART:DURATION=0.50000,URI="part10.mp4"
#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part11.mp4"`
		);
		await fetch.once("x/init.mp4", "init");
		await fetch.once("x/part9.mp4", "");
		await fetch.once("x/part10.mp4", "");
		await fetch.once(
			"x/stream.m3u8?_HLS_msn=0&_HLS_part=2&_HLS_skip=YES",
			`#EXTM3U
#EXT-X-VERSION:9
#EXT-X-MAP:URI="init.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part9.mp4",INDEPENDENT=YES
#EXT-X-PART:DURATION=0.50000,URI="part10.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part11.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part12.mp4"
#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part13.mp4"`
		);
		await fetch.once("x/part11.mp4", "");
		await fetch.once("x/part12.mp4", "");
		hls.destroy();
	});

	test("segmentBoundrary", async () => {
		const $video = new mock.video();
		const fetch = mock.newFetch();
		jest.spyOn(global, "fetch").mockImplementation(fetch.mockFetch);

		const hls = new Hls(testConfig);
		hls.onError = (error) => {
			throw error;
		};

		// @ts-expect-error
		hls.init($video, "x/index.m3u8");
		await fetch.once(
			"x/index.m3u8",
			"#EXTM3U\n" +
				"#EXT-X-VERSION:9\n" +
				'#EXT-X-STREAM-INF:BANDWIDTH=200000,CODECS="avc1.640016,mp4a.40.2"\n' +
				"stream.m3u8"
		);
		await fetch.once(
			"x/stream.m3u8",
			`#EXTM3U
#EXT-X-VERSION:9
#EXT-X-MAP:URI="init.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part9.mp4",INDEPENDENT=YES
#EXT-X-PART:DURATION=0.50000,URI="part10.mp4"
#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part11.mp4"`
		);
		await fetch.once("x/init.mp4", "init");
		await fetch.once("x/part9.mp4", "");
		await fetch.once("x/part10.mp4", "");
		await fetch.once(
			"x/stream.m3u8?_HLS_msn=0&_HLS_part=2&_HLS_skip=YES",
			`#EXTM3U
#EXT-X-VERSION:9
#EXT-X-MAP:URI="init.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part9.mp4",INDEPENDENT=YES
#EXT-X-PART:DURATION=0.50000,URI="part10.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part11.mp4"
seg1.mp4
#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part12.mp4"`
		);
		await fetch.once("x/part11.mp4", "");
		await fetch.once(
			"x/stream.m3u8?_HLS_msn=1&_HLS_part=0&_HLS_skip=YES",
			`#EXTM3U
#EXT-X-VERSION:9
#EXT-X-MAP:URI="init.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part9.mp4",INDEPENDENT=YES
#EXT-X-PART:DURATION=0.50000,URI="part10.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part11.mp4"
seg1.mp4
#EXT-X-PART:DURATION=0.50000,URI="part12.mp4"
#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part13.mp4"`
		);
		hls.destroy();
	});

	test("minVersionMultivariant", async () => {
		const $video = new mock.video();
		const fetch = mock.newFetch();
		jest.spyOn(global, "fetch").mockImplementation(fetch.mockFetch);

		const hls = new Hls(testConfig);
		// @ts-expect-error
		hls.init($video, "x/index.m3u8");

		fetch.once(
			"x/index.m3u8",
			"#EXTM3U\n" +
				"#EXT-X-VERSION:1\n" +
				'#EXT-X-STREAM-INF:BANDWIDTH=200000,CODECS="avc1.640016,mp4a.40.2\n' +
				"stream.m3u8"
		);
		expect(await hls.waitForError()).toEqual(minVersionError);
		hls.destroy();
	});

	test("minVersionMedia", async () => {
		const $video = new mock.video();
		const fetch = mock.newFetch();
		jest.spyOn(global, "fetch").mockImplementation(fetch.mockFetch);

		const hls = new Hls(testConfig);
		// @ts-expect-error
		hls.init($video, "x/index.m3u8");

		await fetch.once(
			"x/index.m3u8",
			"#EXTM3U\n" +
				"#EXT-X-VERSION:9\n" +
				'#EXT-X-STREAM-INF:BANDWIDTH=200000,CODECS="avc1.640016,mp4a.40.2\n' +
				"stream.m3u8"
		);
		fetch.once(
			"x/stream.m3u8",
			"#EXTM3U\n" +
				"#EXT-X-VERSION:1\n" +
				'#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part12.mp4"'
		);
		expect(await hls.waitForError()).toEqual(minVersionError);
		hls.destroy();
	});

	test("missingCodecs", async () => {
		const $video = new mock.video();
		const fetch = mock.newFetch();
		jest.spyOn(global, "fetch").mockImplementation(fetch.mockFetch);

		const hls = new Hls(testConfig);
		// @ts-expect-error
		hls.init($video, "x/index.m3u8");

		await fetch.once(
			"x/index.m3u8",
			"#EXTM3U\n" + "#EXT-X-VERSION:9\n" + "stream.m3u8"
		);
		fetch.once(
			"x/stream.m3u8",
			"#EXTM3U\n" +
				"#EXT-X-VERSION:9\n" +
				'#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part12.mp4"'
		);
		expect(await hls.waitForError()).toEqual(missingCodecsError);
		hls.destroy();
	});

	test("missingMediaURI", async () => {
		const $video = new mock.video();
		const fetch = mock.newFetch();
		jest.spyOn(global, "fetch").mockImplementation(fetch.mockFetch);

		const hls = new Hls(testConfig);
		// @ts-expect-error
		hls.init($video, "x/index.m3u8");

		await fetch.once("x/index.m3u8", "#EXTM3U\n#EXT-X-VERSION:9");
		expect(await hls.waitForError()).toEqual(missingMediaURIError);
		hls.destroy();
	});

	test("recover", async () => {
		const $video = new mock.video();
		const fetch = mock.newFetch();
		jest.spyOn(global, "fetch").mockImplementation(fetch.mockFetch);

		const hlsConfig = {
			maxRecoveryAttempts: 3,
			recoverySleepSec: 0,
		};
		const hls = new Hls(hlsConfig);
		// @ts-expect-error
		hls.init($video, "x/index.m3u8");

		let fatal: Error | undefined;
		hls.onFatal = (error: Error) => {
			fatal = error;
		};
		expect(fatal).toBeUndefined();

		await fetch.once("x/index.m3u8", "#EXTM3U\n#EXT-X-VERSION:9");
		expect(await hls.waitForError()).toEqual(missingMediaURIError);
		expect(fatal).toBeUndefined();

		await fetch.once("x/index.m3u8", "#EXTM3U\n#EXT-X-VERSION:9");
		expect(await hls.waitForError()).toEqual(missingMediaURIError);
		expect(fatal).toBeUndefined();

		await fetch.once("x/index.m3u8", "#EXTM3U\n#EXT-X-VERSION:9");
		expect(await hls.waitForError()).toEqual(missingMediaURIError);
		expect(await hls.waitForFatal()).toEqual(failedAfterRecoveryError);
		hls.destroy();
	});
});

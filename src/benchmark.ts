/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import * as p from "./playlist.js";

export function playlist() {
	const t0 = performance.now();
	console.log("playlist: start");
	for (let i = 0; i < 10000; i++) {
		p.parsePlaylist(mediaPlaylist);
	}
	const t1 = performance.now();
	console.log("playlist: end:", t1 - t0);
}

export function playlistFast() {
	const t0 = performance.now();
	console.log("playlistFast: start");
	for (let i = 0; i < 10000; i++) {
		p.parsePlaylistFast(mediaPlaylist);
	}
	const t1 = performance.now();
	console.log("playlistFast: end:", t1 - t0);
}

const mediaPlaylist = `#EXTM3U
#EXT-X-VERSION:9
#EXT-X-TARGETDURATION:4
#EXT-X-SERVER-CONTROL:CAN-BLOCK-RELOAD=YES,PART-HOLD-BACK=1.45833,CAN-SKIP-UNTIL=24
#EXT-X-PART-INF:PART-TARGET=0.583333334
#EXT-X-MEDIA-SEQUENCE:2
#EXT-X-SKIP:SKIPPED-SEGMENTS=1
#EXT-X-MAP:URI="init.mp4"

#EXTINF:4.00000,
seg3.mp4
#EXT-X-PROGRAM-DATE-TIME:2022-07-01T01:24:49.267Z
#EXT-X-PART:DURATION=0.50000,URI="part9.mp4",INDEPENDENT=YES
#EXT-X-PART:DURATION=0.50000,URI="part10.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part11.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part12.mp4"
#EXT-X-PART:DURATION=0.58333,URI="part13.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part14.mp4"
#EXT-X-PART:DURATION=0.58333,URI="part15.mp4"
#EXT-X-PART:DURATION=0.33333,URI="part16.mp4"
#EXTINF:3.83333,
seg4.mp4
#EXT-X-PROGRAM-DATE-TIME:2022-07-01T01:24:53.268Z
#EXT-X-PART:DURATION=0.58333,URI="part17.mp4",INDEPENDENT=YES
#EXT-X-PART:DURATION=0.50000,URI="part18.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part19.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part20.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part21.mp4"
#EXT-X-PART:DURATION=0.50000,URI="part22.mp4"
#EXT-X-PART:DURATION=0.58333,URI="part23.mp4"
#EXT-X-PART:DURATION=0.33333,URI="part24.mp4"
#EXTINF:3.83333,
seg5.mp4
#EXT-X-PART:DURATION=0.50000,URI="part25.mp4",INDEPENDENT=YES
#EXT-X-PART:DURATION=0.50000,URI="part26.mp4"
#EXT-X-PRELOAD-HINT:TYPE=PART,URI="part27.mp4"`;

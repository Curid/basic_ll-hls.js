/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import * as p from "./playlist.js";

export interface config {
	maxDelaySec?: number;
	maxRecoveryAttempts?: number;
	recoverySleepSec?: number;
}

interface conf {
	maxDelaySec: number;
	maxRecoveryAttempts: number;
	recoverySleepSec: number;
}

const alreadyInitializedError = new Error("already initialized");
const notRunnnigError = new Error("not running");
const abortedError = new Error("aborted");
export const missingMediaURIError = new Error("missing media URI");
export const failedAfterRecoveryError = new Error("failed after recovery attempts");

export default class Hls {
	private readonly config: conf;

	private aborter!: AbortController;
	private partFetcher!: PartFetcher;

	private initialized = false;

	onError!: (error: Error) => void;
	onFatal!: (error: Error) => void;

	constructor(config: config = {}) {
		this.config = fillMissing(config);
	}

	async init($video: HTMLVideoElement, url: string) {
		if (this.initialized) {
			throw alreadyInitializedError;
		}
		this.initialized = true;
		this.aborter = new AbortController();

		const maxAttempts = this.config.maxRecoveryAttempts;
		for (let i = 0; i < maxAttempts || maxAttempts === -1; i++) {
			if (this.aborter.signal.aborted) {
				return;
			}
			try {
				await this.start($video, url);
			} catch (error) {
				if (error !== abortedError && this.onError) {
					if (error instanceof Error) {
						this.onError(error);
					} else {
						this.onError(new Error(String(error)));
					}
				}
				const signal = this.aborter.signal;
				await sleep(this.config.recoverySleepSec * 1000, signal);
				continue;
			}
			return;
		}
		this.destroy();
		if (this.onFatal) {
			this.onFatal(failedAfterRecoveryError);
		}
	}

	async start($video: HTMLVideoElement, url: string) {
		const abortSignal = this.aborter.signal;
		const fetcher = new Fetcher(this.aborter.signal);

		const multiVariant = await fetcher.fetchPlaylist(url);
		validatePlaylist(multiVariant);
		if (multiVariant.mediaURI === undefined) {
			throw missingMediaURIError;
		}

		const baseURL = url.slice(0, url.lastIndexOf("/") + 1);
		const mediaURL = baseURL + multiVariant.mediaURI;
		const media = await fetcher.fetchPlaylist(mediaURL);
		validatePlaylist(media);

		validatePlaylists(multiVariant, media);

		const mimeCodec = `video/mp4; codecs="${multiVariant.codecs}"`;
		const initURL = baseURL + media.init;

		const player = new Player(
			abortSignal,
			fetcher,
			$video,
			initURL,
			mimeCodec,
			this.config.maxDelaySec
		);
		await player.init();

		this.partFetcher = new PartFetcher(
			abortSignal,
			fetcher,
			player,
			baseURL,
			mediaURL
		);
		await this.partFetcher.init(media);
	}

	destroy() {
		this.aborter.abort();
	}

	static isSupported() {
		return "MediaSource" in window;
	}

	// Test helpers.
	async waitForError(): Promise<Error> {
		return new Promise((resolve) => (this.onError = resolve));
	}
	async waitForFatal(): Promise<Error> {
		return new Promise((resolve) => (this.onFatal = resolve));
	}
}

const defaultMaxDelaySec = 0.75;
const defaultMaxRecoveryAttempts = 3;
const defaultRecoverySleepSec = 3;

function fillMissing(config: config): conf {
	if (config.maxDelaySec === undefined) {
		config.maxDelaySec = defaultMaxDelaySec;
	}
	if (config.maxRecoveryAttempts === undefined) {
		config.maxRecoveryAttempts = defaultMaxRecoveryAttempts;
	}
	if (config.recoverySleepSec === undefined) {
		config.recoverySleepSec = defaultRecoverySleepSec;
	}
	return config as conf;
}

const minHLSVersion = 9;

export const minVersionError = new Error(
	`HLS version must be greater or equal to ${minHLSVersion}`
);

function validatePlaylist(playlist: p.playlist) {
	if (playlist.version < minHLSVersion) {
		throw minVersionError;
	}
}

export const missingCodecsError = new Error("missing codecs, #EXT-X-STREAM-INF");

function validatePlaylists(multiVariant: p.playlist, media: p.playlist) {
	if (!multiVariant.codecs && !media.codecs) {
		throw missingCodecsError;
	}
}

class Fetcher {
	private readonly abortSignal: AbortSignal;
	constructor(abortSignal: AbortSignal) {
		this.abortSignal = abortSignal;
	}

	private async fetch(url: string) {
		if (this.abortSignal.aborted) {
			throw abortedError;
		}
		const response = await fetch(url, { signal: this.abortSignal });
		if (response.status < 200 || response.status > 299) {
			throw new Error(`fetch: code=${response.status} url=${url}`);
		}
		return response;
	}

	async fetchPlaylist(url: string): Promise<p.playlist> {
		const response = await this.fetch(url);
		const rawPlaylist = await response.text();
		return p.parsePlaylist(rawPlaylist);
	}

	async fetchPlaylistFast(url: string): Promise<p.playlistFast> {
		const response = await this.fetch(url);
		const rawPlaylist = await response.text();
		return p.parsePlaylistFast(rawPlaylist);
	}

	async fetchVideo(url: string): Promise<ArrayBuffer> {
		const response = await this.fetch(url);
		return await response.arrayBuffer();
	}
}

interface videoFetcher {
	fetchVideo(url: string): Promise<ArrayBuffer>;
}

class Player {
	private readonly abortSignal: AbortSignal;
	private readonly fetcher: videoFetcher;
	private readonly $video: HTMLVideoElement;
	private readonly initURL: string;
	private readonly mimeCodec: string;
	private readonly maxDelaySec: number;

	private initialized = false;

	private mediaSource!: MediaSource;
	private sourceBuffer!: SourceBuffer;

	constructor(
		abortSignal: AbortSignal,
		fetcher: videoFetcher,
		$video: HTMLVideoElement,
		initURL: string,
		mimeCodec: string,
		maxDelaySec: number
	) {
		this.abortSignal = abortSignal;
		this.fetcher = fetcher;
		this.$video = $video;
		this.initURL = initURL;
		this.mimeCodec = mimeCodec;
		this.maxDelaySec = maxDelaySec;

		abortSignal.addEventListener("abort", () => {
			this.destroy();
		});
	}

	async init() {
		if (this.initialized) {
			throw alreadyInitializedError;
		}
		this.initialized = true;

		this.mediaSource = new MediaSource();

		const sourceOpen = this.waitForSourceOpen();
		this.$video.src = URL.createObjectURL(this.mediaSource);
		await sourceOpen;

		this.sourceBuffer = this.mediaSource.addSourceBuffer(this.mimeCodec);

		const updateEnd = this.waitForUpdateEnd();
		const init = await this.fetcher.fetchVideo(this.initURL);
		this.sourceBuffer.appendBuffer(init);
		await updateEnd;
	}

	private destroy() {
		this.mediaSource.endOfStream();
		this.mediaSource.removeSourceBuffer(this.sourceBuffer);
	}

	async fetchAndLoadPart(url: string): Promise<void> {
		if (!this.initialized) {
			throw notRunnnigError;
		}
		if (this.abortSignal.aborted) {
			throw abortedError;
		}
		const buf = await this.fetcher.fetchVideo(url);

		const updateEnd = this.waitForUpdateEnd();
		this.sourceBuffer.appendBuffer(buf);
		await updateEnd;

		if (this.$video.paused) {
			this.$video.play();
		}

		// Enforce max delay.
		const endTS = this.sourceBuffer.buffered.end(0);
		if (this.$video.currentTime < endTS - this.maxDelaySec) {
			// Bump currentTime 50% towards edge of live.
			const diff = endTS - this.$video.currentTime;
			this.$video.currentTime += diff * 0.5;
		}
		//console.log(endTS, this.$video.currentTime);

		// Prune buffer.
		const startTS = endTS - 20;
		if (startTS > 0) {
			this.sourceBuffer.remove(0, startTS);
		}
	}

	private async waitForSourceOpen() {
		return new Promise((resolve) => (this.mediaSource.onsourceopen = resolve));
	}

	private async waitForUpdateEnd() {
		return new Promise((resolve) => (this.sourceBuffer.onupdateend = resolve));
	}
}

interface partLoader {
	fetchAndLoadPart(url: string): Promise<void>;
}

interface playlistFastFetcher {
	fetchPlaylistFast(url: string): Promise<p.playlistFast>;
}

class PartFetcher {
	private readonly abortSignal: AbortSignal;
	private readonly fetcher: playlistFastFetcher;
	private readonly partLoader: partLoader;
	private readonly baseURL: string;
	private readonly mediaURL: string;

	private initialized = false;

	constructor(
		abortSignal: AbortSignal,
		fetcher: playlistFastFetcher,
		partLoader: partLoader,
		baseURL: string,
		mediaURL: string
	) {
		this.abortSignal = abortSignal;
		this.fetcher = fetcher;
		this.partLoader = partLoader;
		this.baseURL = baseURL;
		this.mediaURL = mediaURL;
	}

	async init(media: p.playlist) {
		if (this.initialized) {
			throw alreadyInitializedError;
		}
		if (this.abortSignal.aborted) {
			throw abortedError;
		}
		this.initialized = true;

		if (media.hint === undefined) {
			throw noNextPartError;
		}

		let prevPart!: p.part;

		// Start loading parts at the last independent part.
		const parts = media.parts;
		for (let i = parts.length; i >= 0; i--) {
			if (!parts[i] || !parts[i].independent) {
				continue;
			}
			for (let j = i; j < parts.length; j++) {
				const part = parts[j];
				prevPart = part;
				const url = this.baseURL + part.uri;
				await this.partLoader.fetchAndLoadPart(url);
			}
			break;
		}

		let hint = media.hint;
		while (!this.abortSignal.aborted) {
			const [mediaURL, msn, partIndex] = [this.mediaURL, hint.msn, hint.index];
			const url = `${mediaURL}?_HLS_msn=${msn}&_HLS_part=${partIndex}&_HLS_skip=YES`;
			const media = await this.fetcher.fetchPlaylistFast(url);
			if (media.hint === undefined) {
				throw noNextPartError;
			}
			hint = media.hint;
			const parts = media.parts;
			for (let i = parts.length - 1; i >= 0; i--) {
				if (!parts[i] || !parts[i + 1] || parts[i].uri !== prevPart.uri) {
					continue;
				}
				for (let j = i + 1; j < parts.length; j++) {
					prevPart = parts[j];
					const url = this.baseURL + parts[j].uri;
					await this.partLoader.fetchAndLoadPart(url);
				}
				break;
			}
		}
	}
}

export const noNextPartError = new Error(
	"server returned playlist before part was available"
);

// Sleep that can be aborted.
function sleep(ms: number, signal: AbortSignal) {
	if (signal.aborted) {
		return;
	}
	return new Promise<void>((resolve) => {
		const taskDone = new AbortController();
		const done = () => {
			resolve();
			taskDone.abort();
		};
		const timeout = setTimeout(done, ms);
		signal.addEventListener(
			"abort",
			() => {
				clearTimeout(timeout);
				done();
			},
			{
				once: true,
				signal: taskDone.signal,
			}
		);
	});
}
